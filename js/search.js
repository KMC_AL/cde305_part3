var app = angular.module('myApp', []);
// create value to store login user's information
app.factory('sharedUser', function() {
  var user = "";

  return {
    saveuser: saveuser,
    getuser: getuser
  };

  function saveuser(item) {
    user =item
  }

  function getuser() {
    return user;
  }
});





//set a controll to response the user request
app.controller('customersCtrl', function($scope, $http,sharedUser) {

      
     //delete recipe   
    $scope.delete = function(x){
         console.log("delete list");
                //check if the user is login or not
                if(sharedUser.getuser() !=null && sharedUser.getuser()!="" ){
               console.log(sharedUser.getuser(),"logged in")
                var recipeId= x.recipeId;
                //call api in assignment part2 to delete the recipe record
                 $http({
              url: "https://cde305part2.herokuapp.com/recipes/"+recipeId,
             method: "DELETE"
                
             }).then(
              function (response) {
                  //send a message to user , let user know the record is deleted 
                  window.alert("recipe deleted!");
                     $scope.getList();
             }, function (error) {
   
   
             });
             
             
               
           }
           else{
               //if the user is not login yet show a message to user
               console.log("login first!");
               window.alert("login first!");
           }
         
    }    
    
    
    
    
    
    //update recipe
    $scope.update = function(x){
        console.log("update click");
        //check if the user is login or not
        if(sharedUser.getuser()!=null && sharedUser.getuser()!=""){
            var recipeId = x.recipeId;
            // checking input value  if the value is empty set to old value
            if( $scope.newtitle !=null ||  $scope.newimage !=null || $scope.newurl!=null){
                var data={};
                if($scope.newtitle!="" && $scope.newtitle !=null){
                    data.title=$scope.newtitle
                     $scope.newtitle = null;
                }else{
                    data.title=x.title
                }
                if($scope.newimage!="" && $scope.newimage !=null){
                    data.image=$scope.newimage
                    $scope.newimage = null;
                }else{
                    data.image=x.image
                }
                if($scope.newurl!="" &&  $scope.newurl !=null){
                    data.url=$scope.newurl
                    $scope.newurl = null;
                }else{
                    data.url=x.url
                }
                //ensure the date is not null
                if(data != null){
                    data.userId =x.userId
                    data.recipeId=x.recipeId
                     data = JSON.stringify(data);
                     console.log(data);
                    console.log(x.recipeId);
                    //call api in assignment part 2 to update the recipe data by PUT Method
                $http({
                url: "https://cde305part2.herokuapp.com/recipes/"+x.recipeId,
                  method: "put",
                  data: data
             }).then(
              function (response) {
                  //send a message to user let user know the data is updated
                 window.alert("recipe updated!");
                     $scope.getList();
    
             }, function (error) {
   
   
             });
               
               
           
                $scope.mylist =[];
                    
                }
            }else{
                console.log("enter something new");
            }
            
        }else{
            
            console.log("login first")
            window.alert("login first")
        }
        
    }
    
    //get saved recipe list
    $scope.getList = function(){
        console.log("get list");
        $scope.mylist =[];
        //check if the user is login
        if(sharedUser.getuser() !=null && sharedUser.getuser()!="" ){
               console.log(sharedUser.getuser(),"logged in")
               //call API in assignment Part2 Get method get the saved recipes
               $http.get("https://cde305part2.herokuapp.com/recipes/")
           .then(function (result) {
            for(var i =0 ; i < result.data.recipes.length ; i++){
                if(result.data.recipes[i].userId == sharedUser.getuser() ){
                    console.log(result.data.recipes[i],"matched")
                    $scope.mylist.push(result.data.recipes[i]);
                }
            }
         //   $scope.mylist = result.data.recipes;
            
        console.log($scope.mylist);
        
  
           });
               
               
               
           }
           else{
               //show error message to user if user want to see the list before login
               console.log("To see your saved list, please login first!");
               window.alert("To see your saved list, please login first!");
           }
        
        
    }
       // save a recipe to list
        $scope.save= function(x){
           console.log ("click",x);
           //check if the user is login
           if(sharedUser.getuser() !=null && sharedUser.getuser()!="" ){
               //get data
               console.log(sharedUser.getuser(),"logged in")
               var recipe = {
                   recipeId:x.recipeId,
                   image:x.image,
                   url:x.url,
                   title:x.title,
                   userId:sharedUser.getuser()
                   
               }
               recipe = JSON.stringify(recipe);
               //send a POST requst to API in assignment to save recipe to list
               $http({
              url: "https://cde305part2.herokuapp.com/recipes/",
             method: "POST",
             data: recipe
             }).then(
              function (response) {
                 window.alert("recipe saved!");
                     $scope.getList();
                     console.log(recipe);
             }, function (error) {
   
   
             });
               
               console.log(recipe);
           
                $scope.mylist =[];
              
           }
           else{
               //send a message to ask user to login 
               console.log("To save a recipe,please login first!");
               window.alert("To save a recipe,please login first!");
           }
        }
    
    
                 
                 
 
    
    //search recipe by ingredients
         $scope.myFunc6 = function() {
        console.log("button6 click");
        //check if the keyword is empty
     if(    $scope.keyword !=null){
         //call GET method of API to search recipe by ingredients
         $http.get("https://cde305part2.herokuapp.com/recipes/search/"+$scope.keyword)
        .then(function (result) {
                //save result to data
            $scope.data = result.data.recipes;
         console.log(result)
        console.log($scope.data);
        
  
    });}}
    
   
     $scope.users = [
            //{"email":"a@gmail.com","password":"123"}
         ];
         $scope.signin=true
         
         //signup function
         $scope.signup = function() {
             //get user data
        var user={
            username:$scope.username,
            email:$scope.email,
            password:$scope.password
        }
                
        $scope.emailchecking=[];
        user = JSON.stringify(user);
            //get user info
            $http.get("https://cde305part2.herokuapp.com/users")
           .then(function (result) {
            for(var i =0 ; i < result.data.account.length ; i++){
                //check if the email is dupicate
                if(result.data.account[i].email == $scope.email ){
                    console.log($scope.email," matched")
                    $scope.emailchecking.push(result.data.account[i]);
                }
            }
         //   $scope.mylist = result.data.recipes;
          console.log($scope.emailchecking);
            //after getting user info
           }).then(function(){
               //check if the email can be use
               if(($scope.username!="" || $scope.password!="" || $scope.email=="") && $scope.emailchecking.length ==0 ){
                  $http({
                   url: "https://cde305part2.herokuapp.com/user",
                    method: "POST",
                   data: user
                     }).then(
                function (response) {
                    //show success info to user
                  window.alert("account created!");
                     
             }, function (error) {
   
                 window.alert("singup fail!");
             });
              
        }else{
            //else the email cannot be used, or the information is not compelte
            console.log("information incompelete");
            window.alert("signup fail!");
        }
        
           });
        
        
        
         console.log(user);
   }
   
   
   
   
   //login function
   
    $scope.signin = function() {
         var useremail="";
         console.log("signin click");
         //get user data
       var user={
            username:$scope.username,
            email:$scope.email,
            password:$scope.password
        }
        
        //get user info by username and password
          $http.get("https://cde305part2.herokuapp.com/user?username="+$scope.username+"&password="+$scope.password)
        .then(function (result) {
            //if the result is not null there are user with same password
            if(result.data!= "null"){
            $scope.user = result.data.account[0].username;
            sharedUser.saveuser($scope.user);
                window.alert("login successed!");
                $scope.getList();
            }else{
                sharedUser.saveuser(null);
                 window.alert("login fail");
            }
            console.log("in http");
             console.log(  $scope.user);
        })
        
   }
    
    

    
   
});

    
